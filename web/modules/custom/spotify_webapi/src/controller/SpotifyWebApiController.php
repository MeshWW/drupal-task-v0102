<?php


/**
 * @file
 * Contains \Drupal\spotify_webapi\Controller\SpotifyWebApiController.
 */

namespace Drupal\spotify_webapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\GuzzleException;


/**
 * Spotify Web Api Controller for get bearer token and get artists
 */
class SpotifyWebApiController extends ControllerBase {

  protected $client;

  public function __construct() {
    $this->client = \Drupal::httpClient();
  }

  /**
   * Get bearer token
   *
   * @return mixed|void
   */
  private function getSpotifyToken() {

    try {
      $authorization = $this->client->request('POST', 'https://accounts.spotify.com/api/token', [
        'form_params' => [
          'grant_type' => 'client_credentials',
          'client_id' => '0d5d7866021c4a06b3633d4ac9870a70',
          'client_secret' => 'ae5b0e56ae114d0a83295a8d4a7ace93',
        ],
      ]);

      return $response = json_decode($authorization->getBody());
    } catch (GuzzleException $e) {
      return \Drupal::logger('spotify_webapi')->error($e);
    }

  }

  /**
   *
   * Call spotify artists endpoint
   * @param $ids
   */
  public function getArtists($ids) {

    $auth = $this->getSpotifyToken();

    try {
      $response = $this->client->request('GET', 'https://api.spotify.com/v1/artists/?ids=' . $ids, [
        'headers' => [
          'Authorization' => $auth->token_type . ' ' . $auth->access_token,
        ],
      ]);

      $response = json_decode($response->getBody(), TRUE);
    } catch (GuzzleException $e) {
      return \Drupal::logger('spotify_webapi')->error($e);
    }

    return $response;

  }

  /**
   *
   * Call spotify get-search-item endpoint
   * https://developer.spotify.com/console/get-search-item/
   * search?q=hiphop&type=artist&market=ES&limit=20&offset=5"
   * @param $ids
   */
  public function getSearch($q, $limit) {

    $auth = $this->getSpotifyToken();

    try {
      $response = $this->client->request('GET', 'https://api.spotify.com/v1/search?q='
        . $q . '&type=artist&market=ES&limit=' . $limit . '&offset=5', [
        'headers' => [
          'Authorization' => $auth->token_type . ' ' . $auth->access_token,
        ],
      ]);

      $response = json_decode($response->getBody(), TRUE);
    } catch (GuzzleException $e) {
      return \Drupal::logger('spotify_webapi')->error($e);
    }

    return $response;

  }

  /**
   * Returns a render-able array for artist page.
   */
  public function getArtist($spotifyArtistId) {
    $artist = new SpotifyWebApiController;
    $artist = $artist->getArtists($spotifyArtistId);

    $build['page']['artist'] = [
      '#externalArtistsUrl' => $artist['artists'][0]['external_urls']['spotify'],
      '#artistsName' => $artist['artists'][0]['name'],
      '#SpotifyArtistId' => $artist['artists'][0]['id'],
      '#imgUrl' => $artist['artists'][0]['images'][0]['url'],
      '#SpotifyType' => $artist['artists'][0]['type'],
      '#SpotifyGenres' => $artist['artists'][0]['genres'],
      '#SpotifyPopularit' => $artist['artists'][0]['popularity'],
      '#theme' => 'spotify_artist_page',
    ];
    return $build;
  }

}