<?php
/**
 * @file
 * Contains \Drupal\spotify_webapi\Plugin\Block\SpotifyArtistsBlock.
 */

namespace Drupal\spotify_webapi\Plugin\Block;

use Drupal\spotify_webapi\Controller\SpotifyWebApiController;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'Spotify Artists' Block.
 *
 * @Block(
 *   id = "spotify_artists_block",
 *   admin_label = @Translation("Spotify Artists Block"),
 * )
 */
class SpotifyArtistsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['type_options'] = [
      '#type' => 'value',
      '#spotifyLimit' => [
        '1' => t('1'),
        '2' => t('2'),
        '3' => t('3'),
        '4' => t('4'),
        '5' => t('5'),
        '6' => t('6'),
        '7' => t('7'),
        '8' => t('8'),
        '9' => t('9'),
        '10' => t('10'),
        '11' => t('11'),
        '12' => t('12'),
        '13' => t('13'),
        '14' => t('14'),
        '15' => t('15'),
        '16' => t('16'),
        '17' => t('17'),
        '18' => t('18'),
        '19' => t('19'),
        '20' => t('20'),
      ],
    ];

    $form['search_query'] = [
      '#type' => 'textfield',
      '#title' => t('Search query'),
      '#size' => 10,
      '#maxlength' => 15,
      '#description' => t('Search Spotify for stuff here'),
      '#required' => TRUE,
    ];

    $form['queryLimit'] = [
      '#type' => 'select',
      '#title' => t('Select number of artists to return: Max 20'),
      '#options' => $form['type_options']['#spotifyLimit'],
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['search_query'] = $form_state->getValue('search_query');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $searchResults = new SpotifyWebApiController;
    //$optionsArray = $this->configuration['artistSelect'];

    //$options = array_map($optionsArray);

    $q = $this->configuration['search_query'];;
    $limit = '20';
    $searchResults = $searchResults->getSearch($q, $limit);

    foreach (array_keys($searchResults['artists']['items']) as $key) {
      $artistsName = $searchResults['artists']['items'][$key]['name'];
      $artistsUrl = $searchResults['artists']['items'][$key]['external_urls']['spotify'];
      $spotifyArtistId = $searchResults['artists']['items'][$key]['id'];
      $path = Url::fromRoute('spotify_webapi.artist', ['spotifyArtistId' => $spotifyArtistId],
        ['absolute' => TRUE])->toString();

      $artistData[$key] = [
        '#external_artists_url' => $artistsUrl,
        '#artist_name' => $artistsName,
        '#spotify_artist_id' => $spotifyArtistId,
        '#artist_page_url' => $path,
      ];
    }

    $build = [
      '#search_query' => $q,
      '#limit' => $limit,
      '#artist_data' => $artistData,
      '#theme' => 'spotify_artists_block',
    ];

    return $build;
  }

}

