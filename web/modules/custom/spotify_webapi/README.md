# CyberDuck
### Drupal Task v0102

Jira: https://marcus-oaten.atlassian.net/jira/core/projects/FIRST/list
Code: https://gitlab.com/MeshWW/drupal-task-v0102
Staging site Pantheon: https://dev-drupal-task-v0102.pantheonsite.io/

User 1:
Role: Drupal Admin
User: DrupalAdmin-B1Zc6JjW3
Pass: 52tUVAhKZsXzQkC

User 2
Role: Artist List Admin
User: Artist List Admin
Pass: MJ4cga73PcCzpm9

# The Deliverables
Task environment Setup
Drupal Code base setup: D9
Lando: Pantheon check ARM64 native compatible install:
php: 21.2.0 arm64
database: 5.10.47-linuxkit mysql:aarch64
AArch64 and ARM64 refer to the same thing!
Coding standards compliant: phpCS with drupal/coder
Pantheon Staging environment

Break tasks down in Jira board
Task
# complete the following:

Develop a basic module: 1 module Rest service with block resolver and
Http client Rest:
To setup the module I have used the drupal console.
I have then used the following drupal APIs.
Form API: https://www.drupal.org/docs/drupal-apis/form-api
Block API: https://www.drupal.org/docs/drupal-apis/block-api/block-api-overview
Module Route: https://www.drupal.org/docs/drupal-apis/routing-system/structure-of-routes
And the spotify API
Spotify WebAPI Documentation: https://developer.spotify.com/documentation/web-api/

# Route of links in routing of module:
Block admin form: Fields - Max number of results limited to 1 - 20
I have also created a role for this but could also set it to access content: authenticated.
Role: Artist List Admin
Role fields: spotify login user and password

# Http Client
Connect to Sportify API and bring back a the selected number of artists.
End Point: list of artists
https://developer.spotify.com/console/get-several-artists/
https://developer.spotify.com/documentation/web-api/reference/#/operations/get-multiple-artists

### “Fields - Max number of results limited to 1 - 20” The spotify API only allows to call /artists? for a list of Artist ID’s passed as a parameter max 50 ID’s.
.

To do this quickly in the module I am going to use the GuzzleHttp\Client
https://api.drupal.org/api/drupal/core%21lib%21Drupal.php/function/Drupal%3A%3AhttpClient/9.0.x

# Spotify console
https://developer.spotify.com/console/get-artist/

Block
inside a block and be clickable to open the artist's information in a new page.

Spotify API Documentation: https://developer.spotify.com/documentation/web-api/

Use Drupal 9 -
Create a Drupal module -
The Drupal module should do the following: -
Allow the admin to choose how many Artists they want to display using the FORM API
when placing the block into a region.

The Music Admin shouldn’t be able to select more than 20 artists. -
Connect to ‘Spotify API’ and bring back the selected number of artists. -
Display the Artists in a custom block and be clickable - If the user clicks on the Artist URL it should open a new page and display information about that artist on a custom page using routes. -

The custom page should have its own permission and should only be accessible by logged in users.

https://developer.spotify.com/documentation/web-api/